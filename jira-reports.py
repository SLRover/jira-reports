"""
Search JIRA issues and generate report per user

Types of reports:
    - daily report
    - monthly report

Required data:
    - JIRA URL
    - JIRA username
    - JIRA password
    - search user (optional, default is current username)
    - the reporting day (optional, default is month)
    - the reporting month
    - the reporting year

"""

from jira import JIRA
from openpyxl import Workbook
from calendar import monthrange
import getpass


__jira_conn__ = None


def get_jira_conn():
    """
    Connection to JIRA

    :return: JIRA connection
    """
    global __jira_conn__

    if __jira_conn__ is None:
        __jira_conn__ = JIRA(url, basic_auth=(username, password))

    return __jira_conn__


def get_data(user, day, month, year):
    """
    Get user data from JIRA by API

    :param user: str, JIRA user
    :param day: int
    :param month: int
    :param year: int
    :return: list of JIRA objects
    """
    statuses = 'Open, "To Do", "In Progress", Reopened'
    date = '{0}-{1}-{2}'.format(str(year), str(month), str(day))
    raw_request = '(assignee = {0} OR watcher = {0}) AND status WAS IN ({1}) DURING ("{2}", "{3}")'
    format_request = raw_request.format(user, statuses, date, date)
    data = get_jira_conn().search_issues(format_request)
    return data


def convert_data(data, day):
    """
    Convert list of JIRA objects to JIRA issue.key list

    :param data: list of str
    :param day: int
    :return: list of JIRA issue.key
    """
    issues = list()
    issues.append('Day #' + str(day))
    issues.append(len(data))
    for item in data:
        issues.append(item.key)
    return issues


def write_monthly_file(user, month, year):
    """
    Create Excel file and add monthly values

    :param user: str, search user
    :param month: int
    :param year: int
    :return: generated excel file
    """
    wb = Workbook(write_only=True)
    ws = wb.create_sheet()
    days_range = monthrange(year, month)[1]
    for row in range(days_range):
        raw_data = get_data(user, (row + 1), month, year)
        ws.append(convert_data(raw_data, (row + 1)))
    wb.save('{0}_{1}_{2}.xlsx'.format(user, month, year))
    return print('Monthly report created.')


def write_daily_file(user, day, month, year):
    """
    Create Excel file and add daily values

    :param user: str, search user
    :param day: int
    :param month: int
    :param year: int
    :return: generated excel file
    """
    wb = Workbook(write_only=True)
    ws = wb.create_sheet()
    raw_data = get_data(user, day, month, year)
    ws.append(convert_data(raw_data, day))
    wb.save('{0}_{1}-{2}-{3}.xlsx'.format(user, day, month, year))
    return print('Daily report created.')


if __name__ == '__main__':
    # Get user data
    url = str(input('Enter JIRA URL: '))
    username = input('Enter JIRA username: ')
    password = getpass.getpass(prompt='Enter JIRA password: ', stream=None)
    search_user = input('Enter JIRA search user (if not entered, the username value will be used): ')
    search_day = input('Enter the day (if not entered, a monthly report will be recorded): ')
    search_month = int(input('Enter the month: '))
    search_year = int(input('Enter the year: '))

    # Select user
    if search_user == '':
        search_user = username

    # Select report
    if search_day == '':
        print('Processing...')
        write_monthly_file(search_user, search_month, search_year)
    else:
        print('Processing...')
        write_daily_file(search_user, search_day, search_month, search_year)
