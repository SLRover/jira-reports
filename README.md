# Выгрузка списка задач из JIRA Software

### Основные функции

- Выгрузка списка задач за определенный день по пользователю, который является исполнителем или наблюдателем
- Выгрузка списка задач за месяц по пользователю, который является исполнителем или наблюдателем

### Пример ввода данных

```
Enter JIRA URL: https://jira.selectel.org
Enter JIRA username: vasya
Enter JIRA password: StrongPassword1
Enter JIRA search user (if not entered, the username value will be used): ivan
Enter the day (if not entered, a monthly report will be recorded): 12
Enter the month: 6
Enter the year: 2018
Processing...
Daily report created.
```

Файл с именем _**user_day-month-year.xlsx**_ создается в папке, где выполняется скрипт.

### Требования

Необходимо установить следующие плагины:

- jira
- openpyxl
- calendar
- getpass

### Контакты

E-mail: [sapunov@selectel.ru](sapunov@selectel.ru)

Slack: [sapunov](https://selectel.slack.com/team/UA0MR445A)